#ifndef GETLINE_H_
#define GETLINE_H_

ssize_t getdelim(char **, size_t *, int , FILE *);

ssize_t getline(char **, size_t *,FILE *);

#endif //GETLINE_H_