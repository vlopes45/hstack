#include <stdlib.h>

char * toLowerCase(char* string){
    int i;
    for(i = 0; string[i]; i++){
        string[i] = tolower(string[i]);
    }
    return string;
}